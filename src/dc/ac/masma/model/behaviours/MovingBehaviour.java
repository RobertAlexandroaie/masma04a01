/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dc.ac.masma.model.behaviours;

import dc.ac.masma.model.agents.ReadFileMobileAgent;
import dc.ac.masma.model.utils.ContextConstants;
import jade.core.ContainerID;
import jade.core.behaviours.OneShotBehaviour;

/**
 *
 * @author lab
 */
public class MovingBehaviour extends OneShotBehaviour{

    private String newContainerName;

    public MovingBehaviour(String newContainerName) {
        this.newContainerName = newContainerName;
    }

    public String getNewContainerName() {
        return newContainerName;
    }

    public void setNewContainerName(String newContainerName) {
        this.newContainerName = newContainerName;
    }
    
    @Override
    public void action() {

        myAgent.doMove(new ContainerID(newContainerName, null));
    }
    
}
