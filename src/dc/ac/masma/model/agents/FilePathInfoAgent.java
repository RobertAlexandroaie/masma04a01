
package dc.ac.masma.model.agents;

import dc.ac.masma.model.FileInfo;
import dc.ac.masma.model.behaviours.RespondFileInfoBehaviour;
import jade.core.*;

public class FilePathInfoAgent extends Agent implements TrainedBehaviour{
    
    private FileInfo fileInfo;
    private static final String LOCAL_FILE_NAME = "file.txt";
    
    @Override
    public void setup()
    {
        fileInfo = new FileInfo(LOCAL_FILE_NAME);
    }

    @Override
    public void registerBehaviours() {

        addBehaviour(new RespondFileInfoBehaviour(fileInfo));
    }
}
